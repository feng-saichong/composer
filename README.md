# composer

#### 介绍
单例模式封装es

#### 使用说明

1.  引入  use Fsc123\Fsc\ElasticConnect
2.  将数据放入es中   ElasticConnect::getEslasticInstance()->add('goods',$data); //goods为索引类型(_index)   $data 为要放入的数据
3.  添加索引 中文分词   ElasticConnect::getEslasticInstance()->creation_index('goods','_doc','goods_name');  // goods为索引类型(_index) _doc为索引索引中_type类型(_type)  goods_name为字段名
3.  高亮搜索 ElasticConnect::getEslasticInstance()->search('goods','好东西','goods_name');  //goods为索引类型(_index)  好东西为要搜索的关键字   goods_name为要搜索的字段
4.  删除数据  ElasticConnect::getEslasticInstance()->delData('goods','1');  //goods为索引类型(_index)  1为主键id(_id)